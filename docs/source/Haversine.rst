Haversine
^^^^^^^^^

|

.. image:: https://warehouse-camo.cmh1.psfhosted.org/5ef50fc211dbd0d81f7d9345f3caf843f4927213/68747470733a2f2f75706c6f61642e77696b696d656469612e6f72672f77696b6970656469612f636f6d6d6f6e732f332f33382f4c61772d6f662d686176657273696e65732e737667

|

The haversine formula determines the great-circle distance between two points on a sphere given their longitudes and latitudes. Important in navigation, it is a special case of a more general formula in spherical trigonometry, the law of haversines, that relates the sides and angles of spherical triangles.

The first table of haversines in English was published by James Andrew in 1805, but Florian Cajori credits an earlier use by José de Mendoza y Ríos in 1801. The term haversine was coined in 1835 by James Inman.

These names follow from the fact that they are customarily written in terms of the haversine function, given by haversin(θ) = sin2( θ / 2 ). The formulas could equally be written in terms of any multiple of the haversine, such as the older versine function (twice the haversine). Prior to the advent of computers, the elimination of division and multiplication by factors of two proved convenient enough that tables of haversine values and logarithms were included in 19th and early 20th century navigation and trigonometric texts. These days, the haversine form is also convenient in that it has no coefficient in front of the sin2 function.

source: `wikipedia <https://en.wikipedia.org/wiki/Haversine_formula>`_


|
|
|

Usage:

.. code-block:: console

   >>> from lintang.calc import Haversine

|
|

example:

.. code-block:: console

    >>> from lintang.calc import Haversine
    >>>
    >>> lon1 = -103.548851
    >>> lat1 = 32.0004311
    >>> lon2 = -103.6041946
    >>> lat2 = 33.374939
    >>>
    >>> distance = Haversine.get_distance(lat1, lon1, lat2, lon2)
    >>>
    >>> print(distance)

result:

.. code-block:: console

    >>> 152.96923374692167

Add parameter 'type' to return specific unit of 'kilometers' or 'miles'

.. code-block:: console

    >>> Haversine.get_distance(lat1, lon1, lat2, lon2, type='miles')

default in 'kilometers'