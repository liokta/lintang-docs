Neptu: Date Conversion to Javanese Calendar
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Usage:

.. code-block:: console

   >>> from lintang.calendar import Neptu


example:

.. code-block:: console

   >>> date = Neptu.convert_date(15, 5, 2019)
   >>>
   >>> print(date.get('pasaran'))

result:

.. code-block:: console

   >>> 'Kliwon'

|
|

convert_date parameters:

.. code-block:: console

    convert_date(day, month, year)

    day     : <int> day
    month   : <int> month
    year    : <int> year

|

convert_date will return dictionary:

.. code-block:: console

    {
      'day': <int day>,
      'month': <int month>,
      'month_name': <string month_name>,
      'day_name': <string day_name>,
      'pasaran': <string pasaran>,
      'year': <int year>
    }