.. Lintang documentation master file, created by
   sphinx-quickstart on Sat May 18 16:16:58 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Lintang's documentation!
===================================

Guide
^^^^^

.. toctree::
   :maxdepth: 2

   Installation
   Haversine
   Neptu



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`